<?php
    include_once("config.inc.php");
    include_once("include.php");
    
    $offset = ($page_id - 1) * NUM_PER_PAGE;
    $favorites = $sqldb->query("SELECT * FROM favorites LIMIT ". NUM_PER_PAGE . " OFFSET " . $offset);
    $total_favorite_pages = ceil($favorite_count / floatval(NUM_PER_PAGE));
    $page = "favorite";
?>
<?php include_once("fragments/header.php"); ?>
<table id="columns">
	<tr>
		<td id="main">
			<div id="stream" class="message">
			    <?php if($favorite_count == 0): ?>
		        <h3>目前没有收藏</h3>
			    <?php else: ?>
			    <h3>我的收藏</h3>
			    <?php endif ?>
			    <ol class="self">
			        <?php while($entry = $favorites->fetch(PDO::FETCH_ASSOC)) { ?>
                    <?php $time_string = strftime("%Y-%m-%d %H:%M", strtotime($entry["time"])); ?>
			        <li>
			            <a href="#" class="author"><?php echo $entry["author"]; ?></a>
			            <?php if ($entry["time"]): ?>
			            <span class="content"><?php echo $entry["content"]; ?></span>
			            <span class="stamp">
			                <a href="#" class="time" title="$time_string"><?php echo $time_string; ?></a>
			                <span class="method"><?php echo $entry["method"]; ?></span>
			            </span>
			            <?php else: ?>
    			        <span class="content">此消息已删除或不公开</span>
			            <?php endif ?>
			        </li>
                    <?php } ?>
			    </ol>
			</div>
		    <?php echo paginator($total_favorite_pages, $page_id, $page); ?>
	    </td>
		<?php include_once("fragments/sidebar.php"); ?>
	</tr>
</table>
<?php include_once("fragments/footer.php"); ?>
<?php
    include_once("config.inc.php");
    include_once("include.php");
    
    $offset = ($page_id - 1) * NUM_PER_PAGE;
    $statuses = $sqldb->query("SELECT * FROM statuses LIMIT ". NUM_PER_PAGE . " OFFSET " . $offset);
    $total_status_pages = ceil($status_count / floatval(NUM_PER_PAGE));
    $page = "index";
?>
<?php include_once("fragments/header.php"); ?>
<table id="columns">
    <tr>
        <td id="main">
            <div id="info">
                <div id="avatar">
                    <a href="#" title="更新头像"><img src="static/avatar.jpg" alt="<?php echo NAME; ?>" /></a>
                </div>
                <div id="panel">
                    <h1><?php echo NAME; ?></h1>
                </div>
            </div>
            <div id="content">
                <div id="stream" class="message">
                    <ol>
                        <?php while($entry = $statuses->fetch(PDO::FETCH_ASSOC)) { ?>
                        <?php $time_string = strftime("%Y-%m-%d %H:%M", strtotime($entry["time"])); ?>
                        <li>
                            <span class="content">
                                <?php echo $entry["content"]; ?>
                            </span>
                            <span class="stamp">
                                <a href="#" class="time" title="<?php echo $time_string; ?>" ffid="" stime="<?php echo date("r", strtotime($entry["stime"])); ?>"><?php echo $time_string; ?></a>
                                <span class="method">
                                    <?php echo $entry['method'] ?>
                                </span>
                                <?php if($entry['reply']) {?>
                                <span class="reply"><a href="#">
                                    <?php echo $entry['reply']; ?></a>
                                </span>
                                <?php } ?>
                            </span>
                        </li>
                        <?php } ?>
                    </ol>
                </div>
            <?php echo paginator($total_status_pages, $page_id, $page); ?>
            </div>
        </td>
        <?php include_once("fragments/sidebar.php"); ?>
    </tr>
</table>
<?php include_once("fragments/footer.php"); ?>
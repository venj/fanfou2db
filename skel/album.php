<?php
    include_once("config.inc.php");
    include_once("include.php");
    
    $offset = ($page_id - 1) * PHOTOS_PER_PAGE;
    $photos = $sqldb->query("SELECT * FROM albums LIMIT ". PHOTOS_PER_PAGE . " OFFSET " . $offset);
    $total_photo_pages = ceil($photo_count / floatval(PHOTOS_PER_PAGE));
    $page = "album";
    $counter = 0;
?>
    <?php include_once("fragments/header.php"); ?>

	<div id="body" class="ui-roundedbox"><div class="ui-roundedbox-corner ui-roundedbox-tl"><div class="ui-roundedbox-corner ui-roundedbox-tr"><div class="ui-roundedbox-corner ui-roundedbox-bl"><div class="ui-roundedbox-corner ui-roundedbox-br"><div class="ui-roundedbox-content">
		<div class="inner-content impact">
		<div id="crumb"><a href="status_1.html" class="avatar"><img src="static/avatar_album.jpg" alt="Venj隐退了" /></a><h3>我的照片</h3><h4>共 <?php echo $photo_count; ?> 张 | <a href="index.php">返回我的消息</a></h4></div><div id="album">
		    <table>
                <?php while($entry = $photos->fetch(PDO::FETCH_ASSOC)) { ?>
                    <?php if ($counter == 0): ?>
                    <tr>
                    <?php endif ?>
                    <td><a href="<?php echo $entry["full"]; ?>" class="photo"><img src="<?php echo $entry["thumb"]; ?>" alt="照片" title="<?php echo $entry["title"]; ?>" /></a></td>
                    <?php $counter++; ?>
                    <?php if ($counter == 5): ?>
                    </tr>
                    <?php $counter = 0; ?>
                    <?php endif ?>
		        <?php } ?>
		        </table>
		        <?php echo paginator($total_photo_pages, $page_id, $page); ?>
		        </div><div class="howto"><p class="paipai">推荐使用 <a href="#">饭否拍拍</a> —— 手机客户端，拍照上传更方便！</p></div>		</div>

	</div></div></div></div></div></div>
<?php include_once("fragments/footer.php"); ?>

<?php
    include_once "config.inc.php";
    $page_id = $_GET['page'] ? $_GET['page'] : 1 ;
    $sqldb = new SQLite3("fanfou.sqlite3");
    $status_count = $sqldb->querySingle("SELECT count(*) FROM statuses");
    $privatemsg_receives_count = $sqldb->querySingle("SELECT count(*) FROM privatemsg_receives");
    $privatemsg_sents_count = $sqldb->querySingle("SELECT count(*) FROM privatemsg_sents");
    $photo_count = $sqldb->querySingle("SELECT count(*) FROM albums");
    $favorite_count = $sqldb->querySingle("SELECT count(*) FROM favorites");
    
    function paginator($total_pages, $current_page, $page_type)
    {
        if ($total_pages <= 1)
            return;

        $html = "<ul class=\"paginator\">";
        
        if($total_pages < 7){
            $pages = range(1, $total_pages);
        }
        elseif($current_page <= 3) {
            $pages = range(1, 7);
        }
        elseif($current_page > 3 && ($total_pages - $current_page) > 3) {
            $pages = range($current_page - 3, $current_page + 3);
        }
        else {
            $pages = range($total_pages - 6, $total_pages);
        }
        
        if ($current_page != 1) {
            $prev_id = $current_page - 1;
            $html .= "<li><a href=\"" . $page_type. ".php?page=$prev_id\">上一页</a></li>";
            if (($current_page - 3) > 1 && $total_pages > 7) {
                $html .= "<li>...</li>";
            }
        }
        
        foreach($pages as $page){
            if($page == $current_page) {
                $html .= "<li class=\"current\">$page</li>";
            }
            else {
                $html .= "<li><a href=\"" . $page_type. ".php?page=$page\">$page</a></li>";
            }
        }
        
        if ($current_page != $total_pages) {
            $next_id = $current_page + 1;
            if (($current_page + 3) < $total_pages && $total_pages > 7) {
                $html .= "<li>...</li>";
            }
            $html .= "<li><a href=\"" . $page_type. ".php?page=$next_id\">下一页</a></li>";
        }
        $html .= "</ul>";
        
        return $html;
    }
?>
<td id="sidebar">
    <div id="iconset">
        <?php echo $icons; ?>
    </div>
    <div id="user_infos">
        <ul class="vcard">
            <li id="bio">自述：<span><?php echo DESC; ?></span></li>
        </ul>
    </div>
    <ul id="user_stats">
        <li><a href="#"><span class="count"><?php echo FOLLOWINGS; ?></span> <span class="label">他关注的人</span></a></li>
        <li><a href="#"><span class="count"><?php echo FOLLOWERS; ?></span> <span class="label">关注他的人</span></a></li>
        <li><a href="#"><span class="count"><?php echo (defined("STATUSES") ? STATUSES : $status_count); ?></span> <span class="label">消息</span></a></li>
    </ul>
    <div class="stabs">
        <ul>
            <li <?php echo $page == "index" ? "class=\"current\"" : ""; ?>><a href="index.php"><span class="label">消息</span> <span class="count">(<?php echo $status_count; ?>)</span></a></li>
            <li <?php echo $page == "album" ? "class=\"current\"" : ""; ?>><a href="album.php"><span class="label">照片</span> <span class="count">(<?php echo $photo_count; ?>)</span></a></li>
            <li <?php echo $page == "favorite" ? "class=\"current\"" : ""; ?>><a href="favorite.php"><span class="label">收藏</span> <span class="count">(<?php echo $favorite_count; ?>)</span></a></li>
            <li <?php echo ($page == "privatemsg_receive" or $page == "privatemsg_sent") ? "class=\"current\"" : ""; ?>><a href="privatemsg_receive.php"><span class="label">私信</span> <span class="count">(<?php echo $privatemsg_receives_count; ?>)</span></a></li>
        </ul>
    </div>
    <div class="sect">
        <p class="rssfeed"><a href="#" title="订阅<?php echo NAME; ?>的消息">订阅<?php echo NAME; ?>的消息</a></p>
    </div>
    <div class="sect">
        <div class="user-op">
        </div>
    </div>
</td>
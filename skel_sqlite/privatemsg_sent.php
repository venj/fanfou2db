<?php
    include_once("config.inc.php");
    include_once("include.php");
    
    $offset = ($page_id - 1) * NUM_PER_PAGE;
    $privatemsg_sents = $sqldb->query("SELECT * FROM privatemsg_sents LIMIT ". NUM_PER_PAGE . " OFFSET " . $offset);
    $total_pm_pages = ceil($privatemsg_sents_count / floatval(NUM_PER_PAGE));
    $page = "privatemsg_sent";
?>
<?php include_once("fragments/header.php"); ?>
<table id="columns">
	<tr>
		<td id="main" class="privatemsg">
			            										<div class="tabs">
					<ul>
						<li><a href="privatemsg_receive.php">我收到的私信</a></li>
						<li class="current"><a href="privatemsg_sent.php">我发出的私信</a></li>
					</ul>
				</div>
				<div id="content">
					<div id="stream" class="pm">
					    <ol class="wa">
					        <?php while($entry = $privatemsg_sents->fetchArray()) { ?>
                            <?php $time_string = strftime("%Y-%m-%d %H:%M", strtotime($entry["time"])); ?>
                            <li>
                                发给<a href="#"><?php echo $entry["to"]; ?></a>：<span class="content"><?php echo $entry["content"]; ?></span>
                                <span class="stamp time" title="<?php echo $time_string; ?>"><?php echo $time_string; ?></span>
                                <?php if ($entry["parent"]): ?>
                                <p class="pm-parent"><?php echo $entry["parent"]; ?></p>
                                <?php endif ?>
                            </li>
                            <?php } ?>
					    </ol></div>
					    <?php echo paginator($total_pm_pages, $page_id, $page); ?>
					    </div>
					</td>
		<?php include_once("fragments/sidebar.php"); ?>
	</tr>
</table>
<?php include_once("fragments/footer.php"); ?>
require "rubygems"
require "active_record"

module MeVenjDB
  config = YAML::load_file("database.yml")
  
  ActiveRecord::Base.establish_connection(
    :adapter => config["adapter"], 
    :database => config["database"]
  )
end